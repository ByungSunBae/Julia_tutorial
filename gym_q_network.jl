using Distributions
using OpenAIGym

# Setting Environment of gym
env = GymEnv("CartPole-v0")

# 500 Episode Q-learning
num_eps = 500

# For initializing Q(s,a), Define Hyperparameters
epsilon = 1.
decay = 20000.
j = 0
end_eps = 0.05

lr = 0.001
gamma_val = 0.99

num_h1 = 80
num_h2 = 60

beta_1 = 0.9
beta_2 = 0.999
eps_val = 1e-8

state_dim = size(env.state)[1]
action_dim = length(env.actions.items)

# Define Q agent (2-layer Neural Network, No bias)
rng1 = Normal(0, 1/sqrt(state_dim))
rng2 = Normal(0, 1/sqrt(num_h1))
rng3 = Normal(0, 1/sqrt(num_h2))

W1 = rand(rng1, state_dim, num_h1)
W2 = rand(rng2, num_h1, num_h2)
W_out = rand(rng3, num_h2, action_dim)

# Target Network
W1_tg = copy(W1)
W2_tg = copy(W2)
W_out_tg = copy(W_out)

# For Adam Optimizer
m_W1 = 0.
m_W2 = 0.
m_W_out = 0.

v_W1 = 0.
v_W2 = 0.
v_W_out = 0.

# ReLU activation function
function relu(x)
	return max(x, 0)
end

# score derivate function
function dscore_fn(array)
	@assert length(array) == 2
	return -(array...)
end

# Add Replay Memory
Memory_Q = []
Memory_h1 = []
Memory_h2 = []
Memory_state = []
Max_Size = 1000

batch_size = 32

Copy_Cycle = 500
no_act_time = batch_size * 5

# Repeat (for each episode):
for i=1:num_eps
	reset!(env) # Initialize s
	total_reward = 0
	while true
		cur_state = state(env) # Automatic taking next state

		# Choose action from state using policy derived from Q (e.g., epsilon-greedy)
		if j < no_act_time
			epsilon = 1.
		else
			epsilon = max.(epsilon - 1 / decay, end_eps)
		end
        
		h_1 = relu(transpose(cur_state) * W1)
		h_2 = relu(h_1 * W2)
		Q = h_2 * W_out

		if epsilon > rand()
			act = sample(env.actions.items) # Random Action
		else
			act = indmax(Q) - 1
		end;
		j += 1

		# Take action a, observe r, next state
		r, next_state = step!(env, cur_state, act)
		r_ = env.done ? -50. : r
		next_Q = relu(relu(transpose(next_state) * W1_tg) * W2_tg) * W_out_tg
		Q_c = copy(Q)
		target = env.done ? r_ : r_ + gamma_val * maximum(next_Q)
		Q_c[act+1] = target
		total_reward += r
        
		# Store values for update.
		append!(Memory_Q, [[Q_c, Q]])
		append!(Memory_h1, [h_1])
		append!(Memory_h2, [h_2])
		append!(Memory_state, [cur_state])

		if length(Memory_Q) >= no_act_time
			# Update Q value for actions using back-propagate
			sampled_idx = sample(1:length(Memory_Q), batch_size, replace=false)
			sampled_batch_Q = Memory_Q[sampled_idx]
			sampled_batch_h1 = Memory_h1[sampled_idx]
			sampled_batch_h2 = Memory_h2[sampled_idx]
			sampled_batch_state = Memory_state[sampled_idx]

			dscores = mapreduce(dscore_fn, vcat, sampled_batch_Q)
			batch_h1 = reduce(vcat, sampled_batch_h1)
			batch_h2 = reduce(vcat, sampled_batch_h2)
			batch_state = reduce(hcat, sampled_batch_state)
	         
			dW_out = transpose(batch_h2) * dscores

			dh_2 = dscores * transpose(W_out)
			drelu_h2 = map(x-> x >= 0 ? 1:0, batch_h2)
			dh_2 = dh_2 .* drelu_h2
	        
			dW2 = transpose(batch_h1) * dh_2

			dh_1 = dh_2 * transpose(W2)
			drelu_h1 = map(x-> x >= 0 ? 1:0, batch_h1)
			dh_1 = dh_1 .* drelu_h1
            
			dW1 = batch_state * dh_1
        
			m_W1 = beta_1 * m_W1 + (1 - beta_1)*dW1
			m_W2 = beta_1 * m_W2 + (1 - beta_1)*dW2
			m_W_out = beta_1 * m_W_out + (1 - beta_1)*dW_out            

			v_W1 = beta_2 * v_W1 + (1 - beta_2)*(dW1 .^2)
			v_W2 = beta_2 * v_W2 + (1 - beta_2)*(dW2 .^2)
			v_W_out = beta_2 * v_W_out + (1 - beta_2)*(dW_out .^2)

			hat_m_W1 = m_W1 ./ (1 - beta_1^j)
			hat_m_W2 = m_W2 ./ (1 - beta_1^j)
			hat_m_W_out = m_W_out ./ (1 - beta_1^j)

			hat_v_W1 = v_W1 ./ (1 - beta_2^j)
			hat_v_W2 = v_W2 ./ (1 - beta_2^j)
			hat_v_W_out = v_W_out ./ (1 - beta_2^j)

			W1 = W1 - hat_m_W1 .* (lr ./ sqrt.(hat_v_W1 + eps_val))
			W2 = W2 - hat_m_W2 .* (lr ./ sqrt.(hat_v_W2 + eps_val))
			W_out = W_out - hat_m_W_out .* (lr ./ sqrt.(hat_v_W_out + eps_val))
		else
			next
		end

		if length(Memory_Q) == Max_Size
			map(shift!, [Memory_Q, Memory_h1, Memory_h2, Memory_state])
		end

		if j % Copy_Cycle == 0
			W1_tg = copy(W1)
			W2_tg = copy(W2)
			W_out_tg = copy(W_out)
			info("Copy Network Q ==> Q_target")
		end

		if env.done
			break
		end
	end;
	info("Episode $i Total Reward : $total_reward | Epsilon-Greedy : $epsilon")
end
info("Finished Training")



# Test !!
test_num_eps = 100
for i=1:test_num_eps
	reset!(env)
	total_reward = 0
	while true
		cur_state = state(env) # Automatic taking next state

		# Choose action from state using policy derived from Q (e.g., epsilon-greedy)
        
		h_1 = relu(transpose(cur_state) * W1)
		h_2 = relu(h_1 * W2)
		Q = h_2 * W_out

		act = indmax(Q) - 1
        
		j += 1

		# Take action a, observe r, next state
		r, next_state = step!(env, cur_state, act)
		total_reward += r
       
		if env.done
			break
		end;
	end;
	info("Episode $i Total Reward : $total_reward")
end;
info("Finished Test")

