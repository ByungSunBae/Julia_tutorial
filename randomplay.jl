using Distributions
using OpenAIGym

# Setting Environment of gym
env = GymEnv("CartPole-v0")

# 20 Episode Random Play
num_eps = 20
for i=1:num_eps
    reset!(env)
    total_reward = 0
    while env.done == false
        cur_state = state(env)
        act = sample(env.actions.items) # Random Action
        r, _ = step!(env, cur_state, act)
        total_reward += r
    end
    info("Episode $i Total Reward : $total_reward")
end
